package com.durlov.devicetracker;

import android.Manifest;
import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.BatteryManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.text.DateFormat;
import java.util.Date;

/**
 * Created by moin on 2/20/2016.
 */
public class BackgroundLocationService extends IntentService  implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    GoogleApiClient mGoogleApiClient;
    Location mLastLocation, mCurrentLocation;
    LocationRequest mLocationRequest;
    LocationSettingsRequest.Builder locationSettingsBuilder;
    BackgroundLocationServiceBroadCastReceiver receiver;

    int batteryLevel;

    Boolean mRequestingLocationUpdates = false;
    String mLastUpdateTime = "";

    Boolean highAccuracy  = false, balancedAccuracy  = false, lowAccuracy = false;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public BackgroundLocationService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();

            mGoogleApiClient.connect();
        }

        locationSettingsBuilder = new LocationSettingsRequest.Builder();

        receiver = new BackgroundLocationServiceBroadCastReceiver();
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        registerReceiver(receiver, ifilter);

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(60 * 1000);//1min
        mLocationRequest.setFastestInterval(30 * 1000);//30sec
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationSettingsBuilder.addLocationRequest(mLocationRequest);
    }

    public void postLocation(){
        if(mCurrentLocation != null) {
            JsonObject json = new JsonObject();
            json.addProperty("user_id", 2);
            json.addProperty("lat", mCurrentLocation.getLatitude());
            json.addProperty("lng", mCurrentLocation.getLongitude());
            json.addProperty("accuracy", mCurrentLocation.getAccuracy());
            json.addProperty("timestamp", mCurrentLocation.getTime());
            //json.addProperty("lastGeolocation", new Gson().toJson(mCurrentLocation));
            json.addProperty("provider", mCurrentLocation.getProvider());
            json.addProperty("coords", new Gson().toJson(mCurrentLocation));
            json.addProperty("batteryLevel", batteryLevel);

            Log.d("####json: ", String.valueOf(json));

            /*Ion.with(getApplicationContext())
                    .load("POST", "http://devicetracker.durlov.com/location_add.php")
                    .setJsonObjectBody(json)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            if (e != null) {
                                Log.d("####PostError: ", String.valueOf(e));
                            } else if (result != null) {
                                Log.d("####PostError: ", String.valueOf(result));
                            }
                        }
                    });*/

            Ion.with(getApplicationContext())
                    .load("POST", "http://devicetracker.durlov.com/location_add.php")
                    .setBodyParameter("user_id", String.valueOf(2))
                    .setBodyParameter("lat", String.valueOf(mCurrentLocation.getLatitude()))
                    .setBodyParameter("lng", String.valueOf(mCurrentLocation.getLongitude()))
                    .setBodyParameter("accuracy", String.valueOf(mCurrentLocation.getAccuracy()))
                    .setBodyParameter("timestamp", String.valueOf(mCurrentLocation.getTime()))
                    .setBodyParameter("provider", mCurrentLocation.getProvider())
                    .setBodyParameter("batteryLevel", String.valueOf(batteryLevel))
                    .setBodyParameter("speed", String.valueOf(mCurrentLocation.getSpeed()))
                    .setBodyParameter("coords", new Gson().toJson(mCurrentLocation))
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            if (e != null) {
                                Log.d("####PostError: ", String.valueOf(e));
                            } else if (result != null) {
                                Log.d("####PostSuccess: ", String.valueOf(result));
                            }
                        }
                    });
        }
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (LocationListener) this);
        if (batteryLevel > 70) {
            highAccuracy = true;
            balancedAccuracy = false;
            lowAccuracy = false;
        }else if (batteryLevel < 70 && batteryLevel > 20) {
            highAccuracy = false;
            balancedAccuracy = true;
            lowAccuracy = false;
        }else if (batteryLevel < 20){
            highAccuracy = false;
            balancedAccuracy = false;
            lowAccuracy = true;
        }
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (LocationListener) this);
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            Log.d("###mLastLocation:", mLastLocation.getLatitude() + ", " + mLastLocation.getLongitude());
        }

        mRequestingLocationUpdates = true;
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("####Suspended", "");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d("####Failed", "");
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        postLocation();
        Log.d("###mCurrentLocation:", mCurrentLocation.getLatitude() + ", " + mCurrentLocation.getLongitude());
    }

    public class BackgroundLocationServiceBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            /*int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                    status == BatteryManager.BATTERY_STATUS_FULL;

            int chargePlug = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
            boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
            boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;*/

            batteryLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            /*int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

            float batteryPct = batteryLevel / (float)scale;*/

            /*Log.d("####isCharging", String.valueOf(isCharging));
            Log.d("####usbCharge", String.valueOf(usbCharge));
            Log.d("####acCharge", String.valueOf(acCharge));*/
            //Log.d("####level", String.valueOf(batteryLevel));
            /*Log.d("####scale", String.valueOf(scale));
            Log.d("####batteryPct", String.valueOf(batteryPct));*/

            if (batteryLevel > 70 && highAccuracy == false && mRequestingLocationUpdates) {
                mLocationRequest = new LocationRequest();
                mLocationRequest.setInterval(60 * 1000);//60sec
                mLocationRequest.setFastestInterval(30 * 1000);//30sec
                mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

                stopLocationUpdates();
                startLocationUpdates();
                Log.d("####PowerState: ", "highAccuracy");
            }else if (batteryLevel < 70 && batteryLevel > 20 && (highAccuracy == true || lowAccuracy == true) && mRequestingLocationUpdates) {
                mLocationRequest = new LocationRequest();
                mLocationRequest.setInterval(2*60*1000);//2min
                mLocationRequest.setFastestInterval(60*1000);//1min
                mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

                stopLocationUpdates();
                startLocationUpdates();
                Log.d("####PowerState: ", "balancedAccuracy");
            }else if (batteryLevel < 20 && (highAccuracy == true || balancedAccuracy == true) && mRequestingLocationUpdates){
                mLocationRequest = new LocationRequest();
                mLocationRequest.setInterval(15 * 60 * 1000);//15min
                mLocationRequest.setFastestInterval(10 * 60 * 1000);//10min
                mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);

                stopLocationUpdates();
                startLocationUpdates();
                Log.d("####PowerState: ", "lowAccuracy");
            }

            //com.google.android.gms.common.api.PendingResult<LocationSettingsResult> = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        }
    }
}
