package com.durlov.devicetracker;

/**
 * Created by moin on 2/19/2016.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        /*MainActivity mainActivity = new MainActivity();
        mainActivity.mRequestingLocationUpdates = true;
        mainActivity.startLocationUpdates();*/
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Intent serviceIntent = new Intent(context, MainActivity.class);
            serviceIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            serviceIntent.putExtra("onByBoot", true);
            context.startActivity(serviceIntent);
            /*Intent serviceIntent = new Intent(context, BackgroundLocationService.class);
            context.startService(serviceIntent);*/
        }
        Log.d("####onBootDone", "TRUE");
    }
}
