package com.durlov.devicetracker;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.BatteryManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.text.DateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener  {

    GoogleApiClient mGoogleApiClient;
    Location mLastLocation, mCurrentLocation;
    LocationRequest mLocationRequest;
    LocationSettingsRequest.Builder locationSettingsBuilder;
    MainActivityBroadCastReceiver receiver;

    int batteryLevel;

    int REQUEST_CHECK_SETTINGS = 1;

    Boolean mRequestingLocationUpdates = false;
    String mLastUpdateTime = "";

    Boolean highAccuracy  = false, balancedAccuracy  = false, lowAccuracy = false;

    String REQUESTING_LOCATION_UPDATES_KEY = "REQUESTING_LOCATION_UPDATES_KEY";
    String LAST_UPDATED_TIME_STRING_KEY = "LAST_UPDATED_TIME_STRING_KEY";
    String LOCATION_KEY = "current_location";

    Activity mainActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainActivity = this;

        updateValuesFromBundle(savedInstanceState);

        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();

            Log.d("####mGoogleApiClient:", String.valueOf(mGoogleApiClient));
        }

        locationSettingsBuilder = new LocationSettingsRequest.Builder();

        receiver = new MainActivityBroadCastReceiver();
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        registerReceiver(receiver, ifilter);

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(60*1000);//1min
        mLocationRequest.setFastestInterval(30*1000);//30sec
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationSettingsBuilder.addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, locationSettingsBuilder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                final LocationSettingsStates states = locationSettingsResult.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can
                        // initialize location requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(mainActivity, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    protected void onStart() {
        if(mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        super.onStart();
    }

    protected void onStop() {
        /*
        if(mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }*/
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if(receiver != null) {
            unregisterReceiver(receiver);
        }
        if(mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }

        super.onDestroy();
    }

    public void onStartTrackClick(View view) {
        Button button = (Button) view;
        if (button.getTag().equals("1")) {
            button.setText(getResources().getString(R.string.stop_tracking));
            button.setBackgroundColor(Color.RED);
            button.setTag("2");
            if(mGoogleApiClient.isConnected()) {
                mRequestingLocationUpdates = true;
                startLocationUpdates();
            }else{
                new AlertDialog.Builder(getApplicationContext())
                        .setTitle("Warning")
                        .setMessage("Google API client is not connected yet")
                        .setPositiveButton("Connect", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if(!mGoogleApiClient.isConnecting()){
                                    mGoogleApiClient.connect();
                                }else{
                                    mGoogleApiClient.reconnect();
                                }
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        } else if (button.getTag().equals("2")) {
            button.setText(getResources().getString(R.string.start_tracking));
            button.setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
            button.setTag("1");
            mRequestingLocationUpdates = false;
            stopLocationUpdates();
        }
    }

    public void postLocation(){
        if(mCurrentLocation != null) {
            JsonObject json = new JsonObject();
            json.addProperty("user_id", 1);
            json.addProperty("lat", mCurrentLocation.getLatitude());
            json.addProperty("lng", mCurrentLocation.getLongitude());
            json.addProperty("accuracy", mCurrentLocation.getAccuracy());
            json.addProperty("timestamp", mCurrentLocation.getTime());
            //json.addProperty("lastGeolocation", new Gson().toJson(mCurrentLocation));
            json.addProperty("provider", mCurrentLocation.getProvider());
            json.addProperty("coords", new Gson().toJson(mCurrentLocation));
            json.addProperty("batteryLevel", batteryLevel);

            Log.d("####json: ", String.valueOf(json));

            /*Ion.with(getApplicationContext())
                    .load("POST", "http://devicetracker.durlov.com/location_add.php")
                    .setJsonObjectBody(json)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            if (e != null) {
                                Log.d("####PostError: ", String.valueOf(e));
                            } else if (result != null) {
                                Log.d("####PostError: ", String.valueOf(result));
                            }
                        }
                    });*/

            Ion.with(getApplicationContext())
                    .load("POST", "http://devicetracker.durlov.com/location_add.php")
                    .setBodyParameter("user_id", String.valueOf(1))
                    .setBodyParameter("lat", String.valueOf(mCurrentLocation.getLatitude()))
                    .setBodyParameter("lng", String.valueOf(mCurrentLocation.getLongitude()))
                    .setBodyParameter("accuracy", String.valueOf(mCurrentLocation.getAccuracy()))
                    .setBodyParameter("timestamp", String.valueOf(mCurrentLocation.getTime()))
                    .setBodyParameter("provider", mCurrentLocation.getProvider())
                    .setBodyParameter("batteryLevel", String.valueOf(batteryLevel))
                    .setBodyParameter("speed", String.valueOf(mCurrentLocation.getSpeed()))
                    .setBodyParameter("coords", new Gson().toJson(mCurrentLocation))
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            if (e != null) {
                                e.printStackTrace();
                                //Log.d("####PostError: ", String.valueOf(e));
                            } else if (result != null) {
                                Log.d("####PostSuccess: ", String.valueOf(result));
                            }
                        }
                    });
        }
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (LocationListener) this);
        if (batteryLevel > 70) {
            highAccuracy = true;
            balancedAccuracy = false;
            lowAccuracy = false;
        }else if (batteryLevel < 70 && batteryLevel > 20) {
            highAccuracy = false;
            balancedAccuracy = true;
            lowAccuracy = false;
        }else if (batteryLevel < 20){
            highAccuracy = false;
            balancedAccuracy = false;
            lowAccuracy = true;
        }
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (LocationListener) this);
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            Log.d("###mLastLocation:", mLastLocation.getLatitude() + ", " + mLastLocation.getLongitude());
        }

        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }

        if(getIntent().hasExtra("onByBoot") && getIntent().getExtras().getBoolean("onByBoot") == true){
            onStartTrackClick(findViewById(R.id.startButton));
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("####Suspended", "");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d("####Failed", "");
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        postLocation();
        Log.d("###mCurrentLocation:", mCurrentLocation.getLatitude() + ", " + mCurrentLocation.getLongitude());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("####requestCode: ", String.valueOf(requestCode));
    }

    public class MainActivityBroadCastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            /*int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                    status == BatteryManager.BATTERY_STATUS_FULL;

            int chargePlug = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
            boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
            boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;*/

            batteryLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            /*int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

            float batteryPct = batteryLevel / (float)scale;*/

            /*Log.d("####isCharging", String.valueOf(isCharging));
            Log.d("####usbCharge", String.valueOf(usbCharge));
            Log.d("####acCharge", String.valueOf(acCharge));*/
            //Log.d("####level", String.valueOf(batteryLevel));
            /*Log.d("####scale", String.valueOf(scale));
            Log.d("####batteryPct", String.valueOf(batteryPct));*/

            if (batteryLevel > 70 && highAccuracy == false && mRequestingLocationUpdates) {
                mLocationRequest = new LocationRequest();
                mLocationRequest.setInterval(60 * 1000);//60sec
                mLocationRequest.setFastestInterval(30 * 1000);//30sec
                mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

                stopLocationUpdates();
                startLocationUpdates();
                Log.d("####PowerState: ", "highAccuracy");
            }else if (batteryLevel < 70 && batteryLevel > 20 && (highAccuracy == true || lowAccuracy == true) && mRequestingLocationUpdates) {
                mLocationRequest = new LocationRequest();
                mLocationRequest.setInterval(2*60*1000);//2min
                mLocationRequest.setFastestInterval(60*1000);//1min
                mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

                stopLocationUpdates();
                startLocationUpdates();
                Log.d("####PowerState: ", "balancedAccuracy");
            }else if (batteryLevel < 20 && (highAccuracy == true || balancedAccuracy == true) && mRequestingLocationUpdates){
                mLocationRequest = new LocationRequest();
                mLocationRequest.setInterval(15 * 60 * 1000);//15min
                mLocationRequest.setFastestInterval(10 * 60 * 1000);//10min
                mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);

                stopLocationUpdates();
                startLocationUpdates();
                Log.d("####PowerState: ", "lowAccuracy");
            }

            //com.google.android.gms.common.api.PendingResult<LocationSettingsResult> = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        }
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(REQUESTING_LOCATION_UPDATES_KEY,
                mRequestingLocationUpdates);
        savedInstanceState.putParcelable(LOCATION_KEY, mCurrentLocation);
        savedInstanceState.putString(LAST_UPDATED_TIME_STRING_KEY, mLastUpdateTime);
        super.onSaveInstanceState(savedInstanceState);
    }

    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            // Update the value of mRequestingLocationUpdates from the Bundle, and
            // make sure that the Start Updates and Stop Updates buttons are
            // correctly enabled or disabled.
            if (savedInstanceState.keySet().contains(REQUESTING_LOCATION_UPDATES_KEY)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(REQUESTING_LOCATION_UPDATES_KEY);
            }

            // Update the value of mCurrentLocation from the Bundle and update the
            // UI to show the correct latitude and longitude.
            if (savedInstanceState.keySet().contains(LOCATION_KEY)) {
                // Since LOCATION_KEY was found in the Bundle, we can be sure that
                // mCurrentLocationis not null.
                mCurrentLocation = savedInstanceState.getParcelable(LOCATION_KEY);
            }

            // Update the value of mLastUpdateTime from the Bundle and update the UI.
            if (savedInstanceState.keySet().contains(LAST_UPDATED_TIME_STRING_KEY)) {
                mLastUpdateTime = savedInstanceState.getString(LAST_UPDATED_TIME_STRING_KEY);
            }
        }
    }

}